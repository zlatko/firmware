#include "modules/modules.h"


#include "FreeRTOS.h"
#include "task.h"
#include "queue.h" 
#include "epicardium.h"

#include <stdio.h>

/*Task ID for the Ui handler*/
TaskHandle_t ui_task_id = NULL;

const char* text = "helloworld;";
#define UI_TASK_ITEM_LENGTH 1000

static xQueueHandle ui_tasks;
// uint32_t
// state: do nothing
// state: pairing started
// state: 

typedef struct {
  uint32_t pin;
} UiTaskItem;

enum UiTaskState{
  PAIRING,
  NOTHING
};

enum UiTaskState currentState = NOTHING;

void consumeUiTask(){
  UiTaskItem taskItem;
  if(xQueueReceive(ui_tasks, &taskItem, 0 ) == pdTRUE){
    if (!epic_disp_open()) {
       epic_disp_clear(0xffff);
       char pin[100];
       sprintf(pin,"%ld", taskItem.pin);
       epic_disp_print(0, 0, pin,0, 0xffff);
       epic_disp_update();
       vTaskDelay(1000);
       epic_disp_close();
       currentState = NOTHING;
      }
  }
}

void pairingRequested(uint32_t pin){
  UiTaskItem taskItem;
  taskItem.pin = pin;
  xQueueSend(ui_tasks, &taskItem, 100);
  currentState = PAIRING; 
}

void vUiTask(void *pvParameters)
{
  ui_task_id = xTaskGetCurrentTaskHandle();
  //requires const size
  static uint8_t buffer[sizeof(uint32_t) * UI_TASK_ITEM_LENGTH];   
  static StaticQueue_t queueBuffer;

  ui_tasks = xQueueCreateStatic( UI_TASK_ITEM_LENGTH, sizeof(UiTaskItem), buffer, &queueBuffer);


    while (1) {
      switch(currentState){
        case PAIRING:
          consumeUiTask();
          break;
        case NOTHING:
          vTaskDelay(250);
          break;
      }
    }
}

